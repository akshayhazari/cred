from collections import defaultdict

class User(object):
    users = {}
    cnt = 0

    # set additional attributes to user
    @staticmethod
    def set_attributes(cls,attributes,not_in = None):
        not_in = not_in if not_in else []
        for key in attributes:
            if key not in not_in:
                setattr(cls, key, attributes[key])

    # constructor for User class with mandatory properties and optional properties(attributes)
    def __init__(self,name,age,gender,attributes=None):
        if isinstance(attributes,dict):
            User.set_attributes(self,attributes,['age','name','gender'])
        User.cnt+=1
        self.id = User.cnt
        self.name = name
        self.age = age
        self.gender = gender
        User.users.update({'id':self})

    # update properties/attributes of user
    def update(self,updates):
        if isinstance(updates,dict):
            User.set_attributes(self,updates,['id'])

class Feature(object):
    cnt = 0
    features = {}
    fid = defaultdict(str)

    # This checks if feature with name is already present and raises exception likewise
    def __new__(cls,name,attributes=None):
        if name in Feature.features :
            raise Exception("Feature exits")
        return super(Feature, cls).__new__(cls)

    # This is a function to set additional attributes to feature
    @staticmethod
    def set_attributes(cls,attributes):
        if attributes:
            for key in attributes:
                if key not in ('name','id'):
                    setattr(cls, key, attributes[key])

    # set name and additional attributes to feature
    def __init__(self,name,attributes=None):
        Feature.cnt+=1
        self.id = Feature.cnt
        self.name = name
        Feature.set_attributes(self,attributes)
        Feature.features.update({self.name:self})
        Feature.fid.update({self.id:self.name})

    # Update properties of feature (Just an additional functionality)
    @staticmethod
    def update(updates,name = None,id = None):
        if not name :
            if not id:
                raise Exception("No name or id provided")
            name = Feature.ids[id]
            obj = Feature.features[name]
        else:
            obj = Feature.features[name]
        if isinstance(updates,dict):
            Feature.set_attributes(obj, updates)
            if 'name' in updates:
                obj.name = updates['name']
                Feature.features.pop(name)
                Feature.features[obj.name] = obj

class FeatureGate(object):

    # This method converts properties in conditional expression to values from User attributes eg age = 25
    @staticmethod
    def getValue(a, properties):
        if a in properties:
            a = properties[a.lower()]
        elif isinstance(a, str) and a[0].isdigit():
            v, i = 0, 0
            while i < len(a) and a[i].isdigit():
                v = (v * 10) + int(a[i])
                i += 1
            a = v
        return a

    # To check boolean operator precedence while evaluating expression
    @staticmethod
    def precedence(op):
        if op == 'and' or op == 'or':
            return 1
        if op in ['>=', '<=', '>', '<', '==', '!=']:
            return 2
        if op == 'not':
            return 3
        return 0

    # Applies boolean operator to operands and evaluates sub expression
    @staticmethod
    def applyOp(op, properties, a, b=None):

        a = FeatureGate.getValue(a, properties)
        if b:
            b = FeatureGate.getValue(b, properties)
        op = op.lower()
        if op == 'not':
            return not a
        elif op == 'and':
            return a and b
        elif op == 'or':
            return a or b
        elif op == '>=':
            return a >= b
        elif op == '<=':
            return a <= b
        elif op == '==':
            return a == b
        elif op == '>':
            return a > b
        elif op == '<':
            return a < b
        elif op == '!=':
            return a != b

    # main function to evaluate expression
    @staticmethod
    def exp_eval(expression, properties):
        property, op = [], []

        i, n = 0, len(expression)
        while i < n:
            if expression[i].isspace() or expression[i] in ["'", '"']:
                i += 1
                continue
            elif expression[i] == "(":
                op.append(expression[i])
            elif expression[i].isalnum():
                v = ""
                while i < n and (expression[i].isalnum() or expression[i] in ('_', "-")):
                    v += expression[i]
                    i += 1
                if v.lower() in ("and", "or", "not"):
                    o1 = v.lower()
                    while len(op) > 0 and FeatureGate.precedence(op[-1]) > FeatureGate.precedence(o1):
                        o = op.pop().lower()
                        if o == 'not':
                            v1 = property.pop()
                            v = FeatureGate.applyOp(o, properties, v1)
                        else:
                            v1 = property.pop()
                            v2 = property.pop()
                            v = FeatureGate.applyOp(o, properties, v2, v1)
                        property.append(v)
                    op.append(o1)
                else:
                    property.append(v)
                continue
            elif expression[i] == ")":
                while len(op) > 0 and op[-1] != "(":
                    o = op.pop()
                    if o in ('not'):
                        v1 = property.pop()
                        v = FeatureGate.applyOp(o, properties, v1)
                    else:
                        v1 = property.pop()
                        v2 = property.pop()
                        v = FeatureGate.applyOp(o, properties, v2, v1)
                    property.append(v)
                op.pop()
            else:
                vx = ''
                while expression[i] in ('>', '=', '<', '!'):
                    vx += expression[i]
                    i += 1
                op.append(vx)
                continue
            i += 1
        while len(op) > 0:
            o = op.pop().lower()
            if o == 'not':
                v1 = property.pop()
                v = FeatureGate.applyOp(o, properties, v1)
            else:
                v1 = property.pop()
                v2 = property.pop()
                v = FeatureGate.applyOp(o, properties, v2, v1)
            property.append(v)
        if(len(property)!=1):
            raise Exception("Invalid Conditional Expression")
        return property[-1]

    # feature gate constructor binding Feature object and conditional statement
    def __init__(self,feature,conditions):
        self.feature = feature
        self.conditions = conditions

    # Check if user has access to feature
    def check_access(self,user):
        properties = user.__dict__
        access = FeatureGate.exp_eval(self.conditions,properties)
        return access

if __name__ == "__main__":
    u1 = User("cred1",24,"male",{"past_order_amount":12000})
    f1 = Feature("Same day delivery",{"create_date":'2019-09-10'})
    fg1 = FeatureGate(f1,"""(age > 25 AND not gender != "Male") OR (past_order_amount > 10000)""")
    f2 = Feature("Exclusive access to certain set of categories",{"attribute1":"a1","attribute2":"a2"})
    print(fg1.check_access(u1))
    u2 = User("cred2",29,"female",{"access_on_concurrent_days":56,"my_val":23})

    fg2 = FeatureGate(f2,"""not ((age > 25 AND not gender != "Male")  (my_val <= 10 or access_on_concurrent_days >= 20))""")
    print(fg2.check_access(u2))
