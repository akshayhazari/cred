# Cred

# Installation requirements

pip3 install -r requirements.txt

-----------------------------------------------------

# Running the code

Add the following below (if __name__ == "__main__") to run new cases and execute

python3 fgate.py

1> Usage - Create a user object

u1 = User("cred1",24,"male",{"past_order_amount":12000})

2> Create a Feature Object :

f1 = Feature("Same day delivery",{"create_date":'2019-09-10'})

3> Create a FeatureGate Object binding a conditional expression and Feature object

fg1 = FeatureGate(f1,"""(age > 25 AND not gender != "Male") OR (past_order_amount > 10000)""")

4> Check if user with properties set above has access to feature

print(fg1.check_access(u1))


--------------------------------------------------------

Run unit test cases

run command
sh run_unit_tests.sh
OR
pytest unit_tests.py


