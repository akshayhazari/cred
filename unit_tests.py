from fgate import *
import pytest

def test_same_feature_name():
    with pytest.raises(Exception):
        f1 = Feature("same_name")
        f2 = Feature("same_name")

def test_invalid_conditions():
    f2 = Feature("some_name",{"property":"some_property"})
    with pytest.raises(Exception):
        u2 = User("cred2",29,"female",{"access_on_concurrent_days":56,"my_val":23})
        fg2 = FeatureGate(f2,"""not ((age > 25 AND not gender != "Male") (my_val <= 10 or access_on_concurrent_days >= 20))""")
        fg2.check_access(u2)
    with pytest.raises(Exception):
        u2 = User("cred2",29,"female",{"access_on_concurrent_days":56,"my_val":23})
        fg2 = FeatureGate(f2,"""not ((age > 25 AND not gender != "Male") OR (my_val <= 10 or access_on_concurrent_days >= 20)""")
        fg2.check_access(u2)

def test_valid_conditions():
    f2 = Feature("some_new_name",{"property":"some_property"})
    u2 = User("cred2",29,"female",{"access_on_concurrent_days":56,"my_val":23})
    fg2 = FeatureGate(f2,"""not ((age > 25 AND not gender != "Male") OR (my_val <= 10 or access_on_concurrent_days >= 20))""")
    assert(fg2.check_access(u2)==False)
    fg2 = FeatureGate(f2,"""(age == 29)""")
    assert(fg2.check_access(u2) == True)

